<?php
// Theme name: crusti
// Theme by: Worthapost
// Website: http://www.worthapost.com
// Author name: Mohd. Sakib
//
// Visit our website to rate this theme and see our Ppremium Themes.

$info = array(

  // Pre-defined color schemes.
  'schemes' => array(
    '#860700,#B41C11,#EDC66F,#FFD293,#111111' => t('Red (default)'),
    '#73A91D,#4D7808,#FFE329,#FFF799,#111111' => t('Green'),
    '#052775,#0B3699,#E06D00,#F4C552,#111111' => t('Blue'),
    '#2F84B5,#0D5884,#ed6fb2,#ffc2e1,#111111' => t('Marine'),
    '#D15400,#D07810,#ffcb57,#fff24d,#111111' => t('Orange'),
    '#640093,#8A14C2,#eb00e8,#ffa8fc,#111111' => t('Purple'),
    '#713800,#4A2500,#ffda8a,#ffecd1,#111111' => t('Brown'),
  ),

  // Images to copy over.
  'copy' => array(
    'images/bg.png',
    'images/button.png',
    'images/expanded.png',
	'images/collapsed.png',
	'images/list-border.png',
	'images/list.png',
	'images/logo.png',
	'images/screenshot.png',
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'style.css',
  ),

  // Coordinates of gradient (x, y, width, height).
  'gradient' => array(687, 259, 310, 62),

  // Color areas to fill (x, y, width, height).
  'fill' => array(
    'base' => array(0, 0, 1009, 703),
  ),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(
    'images/header.png'                    => array(10, 59, 5, 200),
    'images/block-corner.png'              => array(678, 335, 320, 30),
    'images/search-bg.png'                 => array(687, 259, 310, 62),
    'images/menu-border.png'                => array(106, 267, 1, 24),
	'images/menu-hover.png'                => array(10, 259, 1, 40),
  ),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#fff',

  // Preview files.
  'preview_image' => 'color/preview.png',
  'preview_css' => 'color/preview.css',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
