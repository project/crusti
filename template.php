<?php


// Theme name: crusti
// Theme by: Worthapost
// Website: http://www.worthapost.com
// Author name: Mohd. Sakib
//
// Visit our website to rate this theme and see our Ppremium Themes.


if (is_null(theme_get_setting('crusti_width'))) {
  global $theme_key;
  // Save default theme settings
  $defaults = array(
    'crusti_type' => 'fluid',
	'crusti_width' => '960'
  );

  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge(theme_get_settings($theme_key), $defaults)
  );
  // Force refresh of Drupal internals
  theme_get_setting('', TRUE);
}

function crusti_comment_wrapper($content, $node) {
  if (!$content || $node->type == 'forum') {
    return '<div id="comments">'. $content .'</div>';
  }
  else {
    return '<h2 class="comments">'. t('Comments') .'</h2>' . '<div id="comments">' . $content .'</div>';
  }
}


function crusti_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();

  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}