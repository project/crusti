Theme by worthapost.

--SETTING FIXED WIDTH
After installing the theme visit theme configuration page (admin/build/themes/settings/crusti) set your desired width.  

--ENABLING SEARCH BOX
1) Enable the search module. Note: search module comes with default Drupal distribution. Just visit admin->build->modules to enable it.
2) Set search permission on permission page.
3) On theme configuration page (admin/build/themes/settings/crusti), make sure "Search box" option is checked.

--CHANGING COLORS
Visit theme configuration page (admin/build/themes/settings/crusti) to change colors
(Gradient will be applicable for search box column instead of header)

--PREMIUM THEMES
Visit http://www.worthapost.com/ to see our high profile themes.

Please rate this theme on our website. This help us understand you better. 

Enjoy!

