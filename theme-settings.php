<?php

function phptemplate_settings($saved_settings) {

  $settings = theme_get_settings('crusti');

  $defaults = array(
    'crusti_type' => 'fluid',
	'crusti_width' => '960'
  );

  $settings = array_merge($defaults, $settings);
  
  $form['crusti_type'] = array(
    '#type' => 'radios',
    '#title' => t('Whether your theme is <strong>fixed</strong> or <strong>fluid width</strong>?'),
    '#default_value' =>  $settings['crusti_type'],
    '#options' => array('fixed' => t('Fixed'), 'fluid' => t('Fluid')),
    '#description' => t('If you set your theme fixed width, see below to set its width in pixels.'),
  );
  
  $form['crusti_width'] = array(
	'#type' => 'textfield',
	'#title' => t('Width of your theme in pixes (applicable only if theme is set to fixed width). Minimum is 800.'),
	'#default_value' => $settings['crusti_width'],
	'#size' => 40,
    '#maxlength' => 4,
  );
  
  return $form;
}


