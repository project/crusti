<?php /*?>
<?php */?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
	<?php if (theme_get_setting('crusti_type') == 'fixed') { 
	$container_width = theme_get_setting('crusti_width');
	 if($container_width < 800){
	 	$container_width = 800;
	 }
	 ?>
	<style type="text/css">
		#container_main{
			width: <?php print $container_width . 'px' ?>;
		}
	</style>
	<?php } ?>
	
	<!--[if lt IE 7]>
    <style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/fix-ie6.css";</style>
		<style type="text/css">
		#container_main.RightsideExist{
			width:expression(document.body.clientWidth > 1260? "1260px": "auto" );
			width:expression(document.body.clientWidth < 800? "800px": "auto" );
		}
		
		#container_main.NoRightside{
			width:expression(document.body.clientWidth > 950? "950px": "auto" );
			width:expression(document.body.clientWidth < 600? "600px": "auto" );
		}
	</style>
	<![endif]-->
    <?php print $scripts ?>
	<!--[if lt IE 7]>
	<script type="text/javascript">
	$(document).ready(function(){
    var RightHeight = getComputedHeight('right');
    $("#content_area").css("height", RightHeight + "px");
	});
	</script>
	<![endif]-->
	<script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>
<body class="<?php print $body_classes; print ' mainbody'; ?>">
	<div id="container_main" <?php  if($right || $subright1 || $subright2) print 'class="RightsideExist"'; else print 'class="NoRightside"'; ?>>
		<div id="container_inner">
			<div id="top_menu" class="clear-block">
			<?php if (isset($secondary_links)) : ?>
				<?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
			<?php endif; ?>
			</div>
			<div id="header">
				<div id="logoandtext">
				<?php if ($logo) { ?><div id="logocontainer"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a></div><?php } ?>
				<?php if($site_name) { ?>
					<div id="texttitles">
					  <?php if ($site_name) { ?><h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
				      <?php if ($site_slogan) { ?><div class='site-slogan'><?php print $site_slogan ?></div><?php } ?>
					</div>
				<?php } ?>
				</div>
			</div>

			<div id="wrap" class="clear-block">
				
				<div id="left" class="clear-block">
					
					<div id="main_menu">
				        <?php if (isset($primary_links)) : ?>
				          <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
				        <?php endif; ?>
					</div>
					
					<div id="content_area">
						
						<?php if($content_top): ?>
						<div id="above_content">
						<?php print $content_top ?>
						</div>
						<?php endif; ?>
						
						<?php if($breadcrumb){ ?><?php print $breadcrumb ?><?php } ?>
						<?php if($title){ ?><h1 class="title"><?php print $title ?></h1><?php } ?>
				        <?php if($tabs){ ?><div class="tabs"><?php print $tabs ?></div> <?php } ?>
				        <?php if ($show_messages) { print $messages; } ?>
				        <?php if($help){ ?><?php print $help ?><?php } ?>
				        <?php print $content; ?>
						
								
						<?php if($under_content): ?>
						<div id="below_content">
						<?php print $under_content ?>
						</div>
						<?php endif; ?>	
							
					</div>
					
				</div>
				
				<?php  if($right || $subright1 || $subright2) { ?>
				<div id="right" class="clear-block" >
					<div id="themesearchbox"><?php if ($search_box) print $search_box; else echo '<div style="padding:20px 8px 0 10px">See Readme.txt to set search box.</div>'; ?></div>
					<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
					
					<div id="main_right">
						<?php print $right ?>
					</div>
					<div id="subright1">
						<?php print $subright1 ?>
					</div>
					<div id="subright2">
						<?php print $subright2 ?>
					</div>
				</div>
				<?php } ?>
				
				<?php  if($bottom1 || $bottom2 || $footer_message) { ?>
				<div id="bottom_blocks" class="clear-block">
					<div id="bottom1">
						<?php print $bottom1 ?>
						&nbsp;
					</div>
					
					<div id="bottom2">
						<?php print $bottom2 ?>
						&nbsp;
					</div>
					<div id="footer">
						<div id="footer_msg">
						<?php print $footer_message ?> <br/>
						<span>Powered by <a href="http://drupal.org/">Drupal</a>, <a href="http://www.worthapost.com/">Worthapost</a>. </span>
						</div>

					</div>
				</div>
				<?php } ?>
				
			</div>
		</div>
	</div> 
<!--
 * Theme name: crusti
 * Theme by: Worthapost
 * Website: http://www.worthapost.com
 * Author name: Mohd. Sakib
 *
 * Visit our website to rate this theme and see our Ppremium Themes.
 *
 */
-->
<?php print $closure ?>
</body>
</html>